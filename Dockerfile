FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:20191002

RUN yum install -y \
    python3-pip \
    python36-devel \
    gcc \
    curl

# Need newer version of Nodejs than is available by default
RUN curl -sL https://rpm.nodesource.com/setup_13.x | bash -
RUN yum install -y nodejs

RUN pip3 install --no-cache --upgrade pip && \
    pip install --no-cache jupyterhub==1.1.0 && \
    pip install --no-cache jupyterlab==1.1.4 && \
    pip install --no-cache notebook==6.0.3 && \
    pip install --no-cache psutil>=5.6.0 && \
    pip install --no-cache numpy && \
    pip install --no-cache nbresuse 

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}
ENV SHELL bash

RUN adduser --uid ${NB_UID} ${NB_USER}

# Guarantee that the contents of the repo are in ${HOME}
COPY *.ipynb ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}

WORKDIR ${HOME}
USER ${USER}
